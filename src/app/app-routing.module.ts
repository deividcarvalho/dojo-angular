import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { UsuarioListagemComponent } from './usuarios/usuario-listagem/usuario-listagem.component';
import { UsuarioCriacaoComponent } from './usuarios/usuario-criacao/usuario-criacao.component';


const routes: Routes = [
  { path: '', component: AppComponent },
  { path: 'usuario', component: UsuarioListagemComponent },
  { path: 'usuario/adicionar', component: UsuarioCriacaoComponent },
  { path: 'usuario/atualizar:id', component: UsuarioListagemComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
