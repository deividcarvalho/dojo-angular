import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from './data.service';

@Injectable()
export class UsuarioService extends DataService{

  constructor(http: HttpClient) { 
    super('https://localhost:44376/api/usuarios', http);
  }
}
