import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import { Observable } from 'rxjs';
import { NotFoundError } from '../errors/not-found-error';
import { BadRequestError } from '../errors/bad-request-error';
import { AppError } from '../errors/app-error';
import { Usuario } from 'src/app/shared/models/usuario';

@Injectable()
export class DataService {
   
   constructor(private url: string, private http: HttpClient) { }

   listar(): any {
      return this.http.get(this.url)
         .catch(this.tratarErros);
   }

   buscar(id) {
      return this.http.get(`${this.url}/${id}`)
         .map((response: Response) => response.json())
         .catch(this.tratarErros);
   }

   adicionar(corpo) {
      return this.http.post(`${this.url}`, corpo)
         .catch(this.tratarErros);
   }

   atualizar(corpo) {
      return this.http.put(`${this.url}/${corpo.id}`, corpo)
         .catch(this.tratarErros);
   }

   remover(id) {
      return this.http.delete(`${this.url}/${id}`)
         .catch(this.tratarErros);
   }

   tratarErros(erro: Response) {
      if (erro.status === 400) {
         return Observable.throw(new BadRequestError());
      }

      if(erro.status === 404){
         return Observable.throw(new NotFoundError());
      }

      return Observable.throw(new AppError(erro));
   }
}