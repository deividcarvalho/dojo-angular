import { Component, OnInit } from '@angular/core';
import { Usuario } from './../../shared/models/usuario';
import { UsuarioService } from 'src/app/core/services/usuario.service';

@Component({
  selector: 'app-usuario-listagem',
  templateUrl: './usuario-listagem.component.html',
  styleUrls: ['./usuario-listagem.component.scss']
})
export class UsuarioListagemComponent implements OnInit {

  constructor(private service: UsuarioService) { }

  usuarios: Usuario[] = [];

  ngOnInit() {
    this.service.listar().subscribe(
      (res: Usuario[] = []) =>
      {
        this.usuarios = res;
      },
    );
  }
  
  removerUsuario(usuario: Usuario) {
    let index = this.usuarios.indexOf(usuario)
    this.usuarios.splice(index, 1);

    this.service.remover(usuario.id)
      .subscribe(
        null,
        err => {
          console.log(err);
          this.usuarios.splice(index, 0, usuario);
        }
      );
  }
}
