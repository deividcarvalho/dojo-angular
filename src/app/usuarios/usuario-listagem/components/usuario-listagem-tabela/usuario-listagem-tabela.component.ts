import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Usuario } from './../../../../shared/models/usuario';
import { faPenAlt, faTrash } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-usuario-listagem-tabela',
  templateUrl: './usuario-listagem-tabela.component.html',
  styleUrls: ['./usuario-listagem-tabela.component.scss']
})
export class UsuarioListagemTabelaComponent {
  
  faPenAlt = faPenAlt
  faTrash = faTrash

  @Input() usuarios: Usuario[];
  @Output() remover: EventEmitter<any> = new EventEmitter();

  removerClick(usuario: Usuario) {
    this.remover.emit(usuario);
  }
}
