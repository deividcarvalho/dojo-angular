import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/shared/models/usuario';
import { UsuarioService } from './../../core/services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usuario-criacao',
  templateUrl: './usuario-criacao.component.html',
  styleUrls: ['./usuario-criacao.component.scss']
})
export class UsuarioCriacaoComponent implements OnInit {

  constructor(private router: Router, private service: UsuarioService) { }

  ngOnInit() {
  }

  adicionarUsuario(usuario: Usuario){
    console.log(usuario)
    this.service.adicionar(usuario)
      .subscribe(
        () => {
          this.router.navigate(['/usuario'])
        },
        err => {
          console.log(err);
        }
      );
  }
}
