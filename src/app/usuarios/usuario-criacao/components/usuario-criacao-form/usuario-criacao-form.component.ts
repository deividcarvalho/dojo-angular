import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-usuario-criacao-form',
  templateUrl: './usuario-criacao-form.component.html',
  styleUrls: ['./usuario-criacao-form.component.scss']
})
export class UsuarioCriacaoFormComponent implements OnInit {

  @Output() submitUsuario: EventEmitter<any> = new EventEmitter();

  form = new FormGroup({
    nome: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    cpf: new FormControl('', [Validators.required, Validators.minLength(11)]),
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit() {}

  get nome() { return this.form.get('nome') as FormControl; }
  get email() { return this.form.get('email') as FormControl; }
  get cpf() { return this.form.get('cpf') as FormControl; }

  adicionar(){
    this.submitUsuario.emit(this.form.value);
  }
}
