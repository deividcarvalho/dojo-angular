import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { NavbarComponent } from './core/layout/navbar/navbar.component';
import { CardComponent } from './shared/components/card/card.component';
import { UsuarioListagemComponent } from './usuarios/usuario-listagem/usuario-listagem.component';
import { UsuarioListagemTabelaComponent } from './usuarios/usuario-listagem/components/usuario-listagem-tabela/usuario-listagem-tabela.component';
import { UsuarioCriacaoComponent } from './usuarios/usuario-criacao/usuario-criacao.component';
import { UsuarioCriacaoFormComponent } from './usuarios/usuario-criacao/components/usuario-criacao-form/usuario-criacao-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UsuarioService } from './core/services/usuario.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CardComponent,
    UsuarioListagemComponent,
    UsuarioListagemTabelaComponent,
    UsuarioCriacaoComponent,
    UsuarioCriacaoFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    UsuarioService
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
